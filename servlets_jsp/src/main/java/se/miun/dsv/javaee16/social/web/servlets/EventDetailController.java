package se.miun.dsv.javaee16.social.web.servlets;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.miun.dsv.javaee16.social.model.Event;
import se.miun.dsv.javaee16.social.service.ILocalSocialService;

/**
 * Servlet implementation class EventDetailController
 */
@WebServlet("/event/view")
public class EventDetailController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AtomicInteger hits;
	@EJB
	private ILocalSocialService ss;
	
	@Override
	public void init() {
		hits = new AtomicInteger(0);
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int eventId = 0;
		try {
			eventId = Integer.parseInt(request.getParameter("id"));
		} catch(NumberFormatException e) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		Event e = ss.findEvent(eventId);
		if(e == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		request.setAttribute("hits", hits.incrementAndGet());
		request.setAttribute("event", e);
		RequestDispatcher rd = null;
		String isAjax = request.getParameter("ajax");
		if(isAjax != null && isAjax.equals("true"))
			//Serve only the fragment for an ajax call
			rd = request.getRequestDispatcher("/WEB-INF/views/eventDetailFragment.jsp");
		else
			rd = request.getRequestDispatcher("/WEB-INF/views/eventDetail.jsp");
		if(rd == null)
			throw new ServletException("Could not acquire request dispatcher");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

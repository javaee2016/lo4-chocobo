package se.miun.dsv.javaee16.social.web;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.ejb.EJB;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import se.miun.dsv.javaee16.social.parsers.event_stream_parser.EventStreamParser;
import se.miun.dsv.javaee16.social.service.ILocalSocialService;

public class ContextListener implements ServletContextListener {
	@EJB
	ILocalSocialService ss;
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) { }

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		//Setup the social service, and put a reference to it in the servlet context
		System.err.println("Populating db");
		InputStream is = null;
		try {
			URL inputFileUrl = new URL("https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt");
			is = inputFileUrl.openStream();
			EventStreamParser esp = new EventStreamParser(is);
			esp.parseEvents();
			ss.addEventGraph(esp.getEvents(), esp.getUsers(), esp.getComments());
		} catch(Exception e) {
			System.err.println("Warning, unable to populate database: " + e.getMessage());
		} finally {
			if(is != null)
				try { is.close(); } catch(IOException e) {}
		}
		System.err.println("Db successfully populated");
	}

}

package se.miun.dsv.javaee16.social.web.functions;

public final class MimeGuesser {
	public static String mimeByFileExtension(String extension) {
		switch(extension.toLowerCase()) {
		case "jpg":
			return "image/jpeg";
		case "png":
			return "image/png";
		case "gif":
			return "image/gif";
		default:
			return null;
		}
	}
	
	public static String fileExtensionByMime(String mime) {
		switch(mime.toLowerCase()) {
		case "image/jpeg":
			return "jpg";
		case "image/png":
			return "png";
		case "image/gif":
			return "gif";
		default:
			return null;
		}		
	}
}
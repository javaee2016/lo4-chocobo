package se.miun.dsv.javaee16.social.web.functions;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

//As inspired by http://stackoverflow.com/questions/35606551/jstl-localdatetime-format
public final class LocalDateTimeFormatter {
	private LocalDateTimeFormatter() {}
	
	public static String formatLocalDateTime(LocalDateTime localDateTime, String pattern) {
		return localDateTime == null ? "" : localDateTime.format(DateTimeFormatter.ofPattern(pattern));
	}
}

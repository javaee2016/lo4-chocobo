package se.miun.dsv.javaee16.social.web.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import se.miun.dsv.javaee16.social.web.functions.MimeGuesser;

/**
 * Servlet implementation class ImageView, currently simplified only to serve
 * user images, can be extended later
 */
@WebServlet("/user/viewImage/*")
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		String fileName = pathInfo.substring(1);
		//Some rudimentary security to prevent escaping the directory
		Pattern p = Pattern.compile("^([a-zA-Z0-9-])+(.){1}(png|jpg|gif)$");
		Matcher m = p.matcher(fileName);
		if(!m.matches()) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		String mime = MimeGuesser.mimeByFileExtension(m.group(3));
		if(mime == null) {
			response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
			return;
		}
		
		String uploadDirectory = getServletContext().getInitParameter("user.imagedir");
		File file = new File(uploadDirectory + fileName);
		if(!file.exists() || !file.isFile() || !file.canRead()) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, file.getAbsolutePath());
			return;
		}
		
		InputStream is = null;
		try {
			response.setContentType(mime);
			is = new FileInputStream(file);
			OutputStream os = response.getOutputStream();
			int read = 0;
			byte[] buf = new byte[1024];
			while((read = is.read(buf)) != -1) {
				os.write(buf, 0, read);
			}
		} finally {
			if(is != null) 
				is.close();
		}
	}
}

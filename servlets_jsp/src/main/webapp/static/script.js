var baseUrl = '/servlets_jsp';

$(document).ready( function() {
	//Event handlers for event overview page
	$('#event-location-filter').on('submit', updateEventOverview);
	$('#event-list span.event-title a').click(showEventDetails);
	
	//Event handlers for event form
	//Draggable users
	$('#user-drag-drop-container li').on('dragstart', dragUserStart);
	$('#user-drag-drop-container li').on('dragend', dragUserEnd);
	//Drop-zones
	$('#user-drag-drop-container ul').on('drop', dropUser);
	$('#user-drag-drop-container ul').on('dragover', function(e) {e.preventDefault();} );
	//The form
	$('#create-event-form').on('submit', prepareEventForm);
	
	$('#upload-user-image-form').on('submit', uploadUserImage);
});

function updateEventOverview(e) {
	e.preventDefault();
	var eventListContainer = $('#event-list-container');
	eventListContainer.animate({
		'backgroundColor': '#666666',
		'opacity': 0.5
	}, 50);
	$.post(baseUrl + '/event/overview', 
			{
				'filter-string': $('#filter-string').val(),
				'ajax': 'true'
			},
			function(data) {
				eventListContainer.html(data);
				eventListContainer.animate({
					'opacity': '1.0',
					'background-color': '#ffffff'
				});
				//Need to bind listeners again
				$('#event-list span.event-title a').click(showEventDetails);
			});
}

function showEventDetails(e) {
	e.preventDefault();
	//The list item that the clicked link lives in
	var li = $(this).parent().parent();
	//Remove any currently displaying event
	$('div .event-details').remove();
	$('#event-list li.active').removeClass('active');
	$.get($(this).attr('href'),
			{'ajax': true},
			function(data) {
				li.append('<div class="event-details">' + data + '</div>');
				li.addClass('active');
				$('div .event-details').slideDown();
			});
}

var originalUserNode;
var dragUserSource;
function dragUserStart(e) {
	originalUserNode = this;
	e.originalEvent.dataTransfer.effectAllowed = 'move';
	e.originalEvent.dataTransfer.setData('text/html', this.outerHTML);
	this.style.opacity = '0.4';
}

function dragUserEnd(e) {
	this.style.opacity = '1.0';
}

function dropUser(e) {
	e.preventDefault();
	//Wrap the target in a jquery node for easier dom manipulation
	var dropTarget = $(this);
	var newElement = $(e.originalEvent.dataTransfer.getData('text/html'));
	
	//For some reason, the new element can inherit the old elements opacity even though its opacity is set after the copy
	//This explicitly sets the opacity of the new element
	newElement.css('opacity', 1.0);
	dropTarget.append(newElement)
	$(originalUserNode).remove();

	//Re-attach event handler
	$('#user-drag-drop-container li').on('dragstart', dragUserStart);
	$('#user-drag-drop-container li').on('dragend', dragUserEnd);
}

function prepareEventForm(e) {
	//Translate the users in the participant drag-drop to elements in select element
	var organizerSelect = $('#organizers');
	$('#organizing-users-list li').each(function() {
		organizerSelect.append('<option value="' + $(this).data('user-id') + '" selected="selected">' + $(this).text() + '</option>');
	});
}

function uploadUserImage(e) {
	e.preventDefault();
	var userImageElement = $('.user-image');
	userImageElement.animate({'opacity': 0.3});
	var form = $(this);
	//Since the ajax request content itself must not be tampered with, inject a hidden field indicating
	//to the server that this is an ajax-request
	form.append('<input type="hidden" name="ajax" value="true" />');
	var formData = new FormData(form[0]);
	$.ajax({
		url: form.attr('action'),
		data: formData,
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		success: function(data) {
			userImageElement.attr('src', baseUrl + '/user/viewImage/' + data).on('load', function() {
				//Stop the fade out, if we were so fast that it is still running
				userImageElement.stop(true, true);
				userImageElement.animate({'opacity': 1.0});
			});
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Error: " + jqXHR.responseText);
			userImageElement.stop(true, true);
			userImageElement.animate({'opacity': 1.0});
		}
	});
}

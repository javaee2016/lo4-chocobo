<%@tag description="Normal template" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="t"%>
<%@attribute name="pageTitle" fragment="true"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><jsp:invoke fragment="pageTitle" /></title>
<c:url value="/static/style.css" var="styleUrl" />
<link rel="stylesheet" href="${styleUrl}"></link>
</head>
<body>
	<div id="header">
		<div id="logo">
			<h1>Social Stuff</h1>
		</div>
		<div id="nav">
			<ul>
				<c:url value="/event/overview" var="eventOverviewUrl" />
				<c:url value="/event/new" var="newEventUrl" />
				<li><a href="${eventOverviewUrl}">Event Overview</a></li>
				<li><a href="${newEventUrl}">Add new Event</a></li>
			</ul>
		</div>
	</div>
	<div id="page-container">
		<jsp:doBody />
		<c:if test="${hits != null}">
			<t:hitCounter hits="${hits}" />
		</c:if>
	</div>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"
		integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<c:url value="/static/script.js" var="scriptUrl" />
	<script src="${scriptUrl }"></script>
</body>
</html>
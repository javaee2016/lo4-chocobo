<%@tag description="Comment tag" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/functions.tld" prefix="cfn"%>
<%@attribute description="The comment object to display" required="true" name="comment"
	type="se.miun.dsv.javaee16.social.model.Comment"%>
<div class="comment">
	<c:url value="/user/view" var="userLink">
		<c:param name="id" value="${comment.author.id}" />
	</c:url>
	<p><a href="${userLink}">${comment.author.firstName} ${comment.author.lastName}</a>, ${cfn:formatLocalDateTime(comment.timeCreated, 'YYYY-MM-dd HH:mm')}<br />
		${comment.commentText}	
	</p>
</div>
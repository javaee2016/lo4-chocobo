<%@tag description="Hit counter" pageEncoding="UTF-8"%>
<%@attribute description="Number of hits to display" required="true" name="hits" type="Integer"%>
<div class="hit-counter">
	${hits} hits on this servlet since the server was restarted.
</div>
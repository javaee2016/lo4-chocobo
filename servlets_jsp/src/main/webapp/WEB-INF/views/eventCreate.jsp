<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="t"%>
<%@taglib uri="/WEB-INF/functions.tld" prefix="cfn"%>
<t:normalWrapper>
	<jsp:attribute name="pageTitle">
Add new event
</jsp:attribute>
	<jsp:body>
	<h1>Add new event</h1>
	<c:if test="${!empty errors}">
		<h2>Errors</h2>
		<p>The following errors occured when trying to create the event</p>
		<ul>
			<c:forEach items="${errors}" var="error">
				<li>${error}</li>
			</c:forEach>
		</ul>
	</c:if>
	<c:url value="/event/new" var="formTarget" />
	<form method="post" action="${formTarget}" id="create-event-form">
		<fieldset id="event-details-fieldset">
		<legend>Event details</legend>
		<p>
			<label for="title">Title</label>
			<input type="text" name="title" id="title" value="${event.title}" />
		</p>
		<p>
			<label for="city">City</label>
			<input type="text" name="city" id="city" value="${event.city}" />
		</p>
		<p>
			<label for="content">Description</label>
			<textarea name="content" id="content" cols="60" rows="4">${event.content}</textarea>
		</p>
		<p>
			<label for="start-time">Start time</label>
			<input type="text" name="start-time" id="start-time" value="${!empty event.startTime ? cfn:formatLocalDateTime(event.startTime, 'YYYY-MM-dd HH:mm') : ''}"/> [YYYY-MM-DD HH:mm]
		</p>
		<p>
			<label for="end-time">End time</label>
			<input type="text" id="end-time" name="end-time" id="end-time" value="${!empty event.endTime ? cfn:formatLocalDateTime(event.endTime, 'YYYY-MM-dd HH:mm') : ''}"/> [YYYY-MM-DD HH:mm]
		</p>
		</fieldset>
		<fieldset id="event-organizers-fieldset">
		<legend>Organizers</legend>
		<p>Drag and drop available users to "Organizing users" to add them to the event</p>
		<div id="user-drag-drop-container">
			<div id="organizing-users-container">
				<h2>Organizing users</h2>
				<ul id="organizing-users-list">
					<c:forEach items="${organizingUsers}" var="user">
						<li data-user-id="${user.id}" draggable="true">${user.lastName}, ${user.firstName}</li>
					</c:forEach>
				</ul>
			</div>
			<div id="available-users-container">
				<h2>Available users</h2>
				<ul id="available-users-list">
					<c:forEach items="${availableUsers}" var="user">
						<li data-user-id="${user.id}" draggable="true">${user.lastName}, ${user.firstName}</li>
					</c:forEach>
				</ul>
			</div>
		</div>
		<%-- Will be populated by jquery from drag-drop interface on form submit --%>
		<select multiple="multiple" name="organizers" id="organizers"></select>
		</fieldset>
		<fieldset class="action">
		<legend>Actions</legend>
			<input type="submit" value="Add" id="add-event" name="add-event" />
		</fieldset>
	</form>
</jsp:body>
</t:normalWrapper>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/functions.tld" prefix="cfn"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="t"%>
<t:normalWrapper>
	<jsp:attribute name="pageTitle">
Event Overview
</jsp:attribute>
	<jsp:body>
<h1>Event Overview</h1>
<c:url value="/event/overview" var="formTarget" />
<form action="${formTarget}" method="POST" id="event-location-filter">
<label for="filter-string">Location filter: </label>
<input id="filter-string" name="filter-string" type="text" 
	value="${param['filter-string'] == null ? '' : param['filter-string']}">
</form>
<div id="event-list-container">
	<jsp:include page="/WEB-INF/views/eventListFragment.jsp" />
</div>
</jsp:body>
</t:normalWrapper>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/functions.tld" prefix="cfn"%>

<c:choose>
<c:when test="${!empty events}">
<ul id="event-list">
	<c:forEach items="${events}" var="event">
		<li>
				<c:url value="/event/view" var="eventLink">
					<c:param name="id" value="${event.id}" />
				</c:url>
				<span class="event-title"><a href="${eventLink}"><c:out value="${event.title}" /></a></span>
				<span class="event-city"><c:out value="${event.city}" /></span> 
				<span class="event-date"><c:out value="${cfn:formatLocalDateTime(event.startTime, 'YYYY-MM-dd')}" /></span>
		</li>
	</c:forEach>
</ul>
</c:when>
<c:otherwise>
<p>No matching events</p>
</c:otherwise>
</c:choose>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/functions.tld" prefix="cfn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="t"%>

<%-- Fragment for detail view of event (fragmentized for possible future AJAX use) --%>

<h2 class="event-title">${event.title}</h2>
<p class="event-content">${event.content}</p>
<p>
	<span class="event-location">Where: ${event.city}<br /></span>
	<span class="event-time">When: ${cfn:formatLocalDateTime(event.startTime, 'YYYY-MM-dd HH:mm')} to ${cfn:formatLocalDateTime(event.endTime, 'YYYY-MM-dd HH:mm')}<br /></span>
	<span class="event-updated">Last updated: ${cfn:formatLocalDateTime(event.lastUpdate, 'YYYY-MM-dd HH:mm')}</span>
</p>
<h3 class="event-organizers">Organizers</h3>
<ul class="event-organizer-list">
	<c:forEach items="${event.organizers}" var="organizer">
		<li>
			<c:url value="/user/view" var="userLink">
				<c:param name="id" value="${organizer.id}" />
			</c:url> 
			<a href="${userLink}">${organizer.firstName} ${organizer.lastName}</a>
		</li>
	</c:forEach>
</ul>
<c:if test="${!empty event.comments}">
<h3>Comments</h3>
<c:forEach items="${event.comments}" var="comment" >
	<t:comment comment="${comment}" />
</c:forEach>
</c:if>	

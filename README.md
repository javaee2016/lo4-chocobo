# README #

## Requirements ##
* Payara 4.1 (with PostgreSQL driver installed, connection pool set up and connection resource setup with name jdbc/jee16)
* PostgreSQL
* Maven

## Instructions ##
Import the parent pom.xml (and all child-projects) as an existing maven project in Eclipse. Ensure you have Glassfish/Payara set up in Eclipse, and add "social_ear" to the server.

In order for image uploads to work, edit web.xml (user.imagedir) in the servlets_jsp project.

More instructions to come, feel free to add your own.
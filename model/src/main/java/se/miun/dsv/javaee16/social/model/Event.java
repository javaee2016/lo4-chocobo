package se.miun.dsv.javaee16.social.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Event in the javaee16 social application
 * @author frni1203
 */

public class Event implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private String title;
	private String city;
	private String content;
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private LocalDateTime lastUpdate;
	private Set<User> organizers;
	private Set<Comment> comments;
	
	public Event() {
		this.organizers = new HashSet<>();
		this.comments = new HashSet<>();
	}
	
	/**
	 * Public ctor
	 * @param title Title of the event
	 * @param city Name of the city the event takes place in
	 * @param content Event description
	 * @param startTime Start time for the event
	 * @param endTime End time for the event
	 * @param organizers List of users organizing the event (can also be null, and organizers later set with addOrganizers)
	 */
	public Event(String title, String city, String content, LocalDateTime startTime, LocalDateTime endTime, Set<User> organizers) {
		this.title = title;
		this.city = city;
		this.content = content;
		this.startTime = startTime;
		this.endTime = endTime;
		this.organizers = organizers;
		this.comments = new HashSet<Comment>();
	}
	
	public String getCity() {
		return city;
	}
	
	public Set<Comment> getComments() {
		return comments;
	}
	
	public String getContent() {
		return content;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public long getId() {
		return id;
	}
	
	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}
	
	public Set<User> getOrganizers() {
		return organizers;
	}
	
	public LocalDateTime getStartTime() {
		return startTime;
	}
	
	public String getTitle() {
		return title;
	}
	
	@SuppressWarnings("unused")
	private void setLastUpdateTimeNow() {
		setLastUpdate(LocalDateTime.now());
	}
		
	public boolean removeOrganizer(User organizer) {
		return organizers.remove(organizer);
	}
	
	public void setCity(String city) {
		this.city = city;
	}

	@SuppressWarnings("unused")
	private void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
	
	@SuppressWarnings("unused")
	private void setId(long id) {
		this.id = id;
	}
	
	private void setLastUpdate(LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public void setOrganizers(Set<User> organizers) {
		this.organizers = organizers;
	}
	
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
}

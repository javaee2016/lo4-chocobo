package se.miun.dsv.javaee16.social.service;

import java.util.List;
import java.util.Set;

import javax.ejb.Local;

import se.miun.dsv.javaee16.social.model.Comment;
import se.miun.dsv.javaee16.social.model.Event;
import se.miun.dsv.javaee16.social.model.User;

@Local
public interface ILocalSocialService {

	void addUser(User u);

	void addEvent(Event ev);

	void addEventGraph(Set<Event> events, Set<User> users, Set<Comment> comments);

	List<Event> findAllEvents();

	Event findEvent(long eventId);

	User findUser(long userId);

	List<Event> findEventsInCity(String cityName);

	List<User> findAllUsers();

	void saveUser(User u);

}
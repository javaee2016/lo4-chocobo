package se.miun.dsv.javaee16.social.service;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.miun.dsv.javaee16.social.model.Comment;
import se.miun.dsv.javaee16.social.model.Event;
import se.miun.dsv.javaee16.social.model.User;

@Stateless
public class SocialServiceBean implements ILocalSocialService {
	@PersistenceContext(unitName = "social")
	private EntityManager em;

	@Override
	public void addUser(User u) {
		em.persist(u);
	}

	@Override
	public void addEvent(Event ev) {
		em.persist(ev);
	}

	@Override
	public void addEventGraph(Set<Event> events, Set<User> users, Set<Comment> comments) {
		for (User u : users) {
			em.persist(u);
		}
		for (Event e : events) {
			em.persist(e);
		}
		for (Comment c : comments) {
			em.persist(c);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Event> findAllEvents() {
		return (List<Event>) em.createQuery("SELECT e FROM Event e ORDER BY e.startTime").getResultList();
	}

	@Override
	public Event findEvent(long eventId) {
		return em.find(Event.class, eventId);
	}

	@Override
	public User findUser(long userId) {
		return em.find(User.class, userId);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Event> findEventsInCity(String cityName) {
		return (List<Event>) em.createQuery("SELECT e FROM Event e WHERE lower(e.city) LIKE :cityName")
				.setParameter("cityName", (cityName + "%").toLowerCase()).getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<User> findAllUsers() {
		return (List<User>) em.createQuery("SELECT u FROM User u ORDER BY u.lastName").getResultList();
	}

	@Override
	public void saveUser(User u) {
		em.merge(u);
	}
}
